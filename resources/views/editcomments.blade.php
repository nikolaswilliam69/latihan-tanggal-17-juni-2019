<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="{{asset('css/form.css')}}">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <meta http-equiv="content-type" content="text/html; charset= UTF-8">
        <title>Edit your Comment</title>
    </head>
    <body>
        <form action="{{route('comment.update', $comment->id)}}" method="POST" enctype="multipart/form-data">
        @method('PATCH')
            @csrf
            <div class="container">
            @if($errors->any())
                <div class="alert alert-danger">
                <ul>
                @foreach($errors->all() as $messages)
                    <li>{{$messages}}</li>
                @endforeach
                </ul>
                </div>
            @endif
                <label for="username">UserName:</label>
                <input type="text" id="username" placeholder="your username" value="{{$comment->username}}" name="username">
                <label for="comment">Comment:</label>
                <input type="text" id="comment" placeholder="Please enter your comment" value="{{$comment->comment}}" name="comment">
                <input type="submit" value="Edit Comment">
            </div>
        </form>
    </body>
</html>