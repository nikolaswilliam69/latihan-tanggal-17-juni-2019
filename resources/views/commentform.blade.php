<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="{{asset('css/form.css')}}">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <meta http-equiv="content-type" content="text/html; charset= UTF-8">
        <title>Add your Comment</title>
    </head>
    <body>
        <form action="{{route('comment.add')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="container">
            @if($errors->any())
                <div class="alert alert-danger">
                <ul>
                @foreach($errors->all() as $messages)
                    <li>{{$messages}}</li>
                @endforeach
                </ul>
                </div>
            @endif
                <label for="inputmovietitle">Movie Title</label>
                <select name="movie_id" id="">
                    @foreach($movies as $movie)
                    <option value="{{$movie->id}}">{{$movie->title}}</option>
                    @endforeach
                </select>
                <label for="username">UserName:</label>
                <input type="text" id="username" placeholder="your username" name="username">
                <label for="comment">Comment:</label>
                <input type="text" id="comment" placeholder="Please enter your comment" name="comment">
                <input type="submit" value="Add Comment">
            </div>
        </form>
    </body>
</html>