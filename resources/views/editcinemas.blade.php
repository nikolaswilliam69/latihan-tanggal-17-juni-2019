<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="{{asset('css/form.css')}}">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <meta http-equiv="content-type" content="text/html; charset= UTF-8">
        <title>Edit your Cinema</title>
    </head>
    <body>
        <form action="{{route('cinema.update', $cinema->id)}}" method="POST" enctype="multipart/form-data">
        @method('PATCH')
            @csrf
            <div class="container">
            @if($errors->any())
                <div class="alert alert-danger">
                <ul>
                @foreach($errors->all() as $messages)
                    <li>{{$messages}}</li>
                @endforeach
                </ul>
                </div>
            @endif
                <label for="cinema">Nama Bioskop:</label>
                <input type="text" id="cinema" placeholder="Nama Bioskop" name="cinema">
                <label for="address">Alamat:</label>
                <input type="text" id="address" placeholder="Alamat Bioskop" name="address">
                <input type="submit" value="Add Cinema">
            </div>
        </form>
    </body>
</html>