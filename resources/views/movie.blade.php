<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Movie</title>
    <link rel="stylesheet" href="{{'css/app.css'}}">
</head>
<body>
    <div class="container">
        @if(Session::has('success'))
        <div class="alert alert-success">
            <p>{{Session::get('success')}}</p>
        </div>
        @endif
        @if(Session::has('error'))
        <div class="alert alert-danger">
            <p>{{Session::get('error')}}</p>
        </div>
        @endif
        <table class="table">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Sinopsis</th>
                    <th>Director</th>
                    <th>Date Releases</th>
                    <th>Image</th>
                    <th>Bioskop</th>
                </tr>
            </thead>
            <tbody>
                @foreach($movies as $movie)
                <tr>
                    <td>{{$movie->title}}</td>
                    <td>{{$movie->sinopsis}}</td>
                    <td>{{$movie->director}}</td>
                    <td>{{$movie->date}}</td>
                    <td><img src = "{{asset('Storage/images/'.$movie->image)}}" alt ="gambar" height="150" width="100"></td>
                    <td><form action="{{route('movie.delete', $movie->id)}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Delete</button> 
                    </form></td>
                    <td><a href="{{route('movie.edit', $movie->id)}}" class="btn btn-success">Edit</a></td>
                    <td>{{$movie->cinema}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{$movies->links()}}
    </div>
</body>
</html>