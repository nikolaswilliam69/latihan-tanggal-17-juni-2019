<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/show.css')}}">
    <title>Movie Details</title>
</head>
<body>
    <div class="container">
        <div class="d-flex justify-content-center align-item-center">
            <img src="{{asset('Storage/images/'.$movie->image)}}" alt="" height="500" width="400">
        </div>
        <div class="sinopsis">
            <p>{{$movie->sinopsis}}</p>
        </div>
        <div class="comment">
            <h1>Comment</h1>
        </div>
        <div class="row">
        @foreach($movie->comments as $comment)
        <div class="content">
        <p>{{$comment->comment}}</p>
        </div>
        @endforeach
        </div>
    </div>
</body>
</html>