<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="{{asset('css/form.css')}}">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <meta http-equiv="content-type" content="text/html; charset= UTF-8">
        <title>Edit your Movie</title>
    </head>
    <body>
        <form action="{{route('movie.update', $movie->id)}}" method="POST" enctype="multipart/form-data">
        @method('PATCH')
            @csrf
            <div class="container">
            @if($errors->any())
                <div class="alert alert-danger">
                <ul>
                @foreach($errors->all() as $messages)
                    <li>{{$messages}}</li>
                @endforeach
                </ul>
                </div>
            @endif
                <label for="title">Title:</label>
                <input type="text" id="title" placeholder="Movie Title" value="{{$movie->title}}" name="title">
                <label for="sinopsis">Sinopsis:</label>
                <input type="text" id="sinopsis" placeholder="Movie Sinopsis" value="{{$movie->sinopsis}}" name="sinopsis">
                <label for="director">Director:</label>
                <input type="text" id="director" placeholder="Name of the Director" value="{{$movie->director}}" name="director">
                <label for="date">Date Releases:</label>
                <input type="text" id="date" placeholder="Movie Date Release" value="{{$movie->date}}" name="date">
                <label for="image">Movie Image:</label>
                <input type="file" id="image" placeholder="Movie Image" name="image">
                <input type="submit" value="Edit Movie">
            </div>
        </form>
    </body>
</html>