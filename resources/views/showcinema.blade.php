<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/show.css')}}">
    <title>Cinema Details</title>
</head>
<body>
    <div class="container">
        <div class="d-flex justify-content-center align-item-center">
            <img src="{{asset('Storage/images/'.$movie->image)}}" alt="" height="500" width="400">
        </div>
        <div class="sinopsis">
            <p>{{$cinema->address}}</p>
        </div>
        <div class="movie">
            <h1>Film yang sedang Tayang</h1>
        </div>
        <div class="row">
        @foreach($cinema->movies as $movie)
        <div class="content">
        <p>{{$movie->movie}}</p>
        </div>
        @endforeach
        </div>
    </div>
</body>
</html>