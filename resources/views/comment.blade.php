<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Comment</title>
<link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
    <div class="container">
        @if(Session::has('success'))
        <div class="alert alert-success">
            <p>{{Session::get('success')}}</p>
        </div>
        @endif
        @if(Session::has('error'))
        <div class="alert alert-danger">
            <p>{{Session::get('error')}}</p>
        </div>
        @endif
        <table class="table">
            <thead>
                <tr>
                    <th>Movie TItle</th>
                    <th>Username</th>
                    <th>Comment</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($comments as $comment)
                <tr>
                    <td>{{$comment->movie->title}}</td>
                    <td>{{$comment->username}}</td>
                    <td>{{$comment->comment}}</td>
                    <td><a href="{{route('comment.edit', $comment->id)}}" class="btn btn-success">Edit</a></td>
                    <td><form action="{{route('comment.delete', $comment->id)}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Delete</button> 
                    </form></td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{$comments->links()}}
    </div>
</body>
</html>