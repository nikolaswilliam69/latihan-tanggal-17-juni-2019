<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="{{asset('css/form.css')}}">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <meta http-equiv="content-type" content="text/html; charset= UTF-8">
        <title>Add your Movie</title>
    </head>
    <body>
        <form action="{{route('movie.add')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="container">
            @if($errors->any())
                <div class="alert alert-danger">
                <ul>
                @foreach($errors->all() as $messages)
                    <li>{{$messages}}</li>
                @endforeach
                </ul>
                </div>
            @endif
                <label for="title">Title:</label>
                <input type="text" id="title" placeholder="Movie Title" name="title">
                <label for="sinopsis">Sinopsis:</label>
                <input type="text" id="sinopsis" placeholder="Movie Sinopsis" name="sinopsis">
                <label for="director">Director:</label>
                <input type="text" id="director" placeholder="Name of the Director" name="director">
                <label for="date">Date Releases:</label>
                <input type="text" id="date" placeholder="Movie Date Release" name="date">
                <label for="image">Movie Image:</label>
                <input type="file" id="image" placeholder="Movie Image" name="image">
                <input type="submit" value="Add Movie">
            </div>
        </form>
    </body>
</html>