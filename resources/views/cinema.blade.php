<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cinema</title>
<link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
    <div class="container">
        @if(Session::has('success'))
        <div class="alert alert-success">
            <p>{{Session::get('success')}}</p>
        </div>
        @endif
        @if(Session::has('error'))
        <div class="alert alert-danger">
            <p>{{Session::get('error')}}</p>
        </div>
        @endif
        <table class="table">
            <thead>
                <tr>
                    <th>Nama Bioskop</th>
                    <th>Alamat</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($cinemas as $cinema)
                <tr>
                    <td>{{$cinema->cinema}}</td>
                    <td>{{$cinema->address}}</td>
                    <td><a href="{{route('cinema.edit', $cinema->id)}}" class="btn btn-success">Edit</a></td>
                    <td><form action="{{route('cinema.delete', $cinema->id)}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Delete</button> 
                    </form></td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{$cinemas->links()}}
    </div>
</body>
</html>