<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cinema extends Model
{
    protected $fillable = [
        'cinema', 'address'
    ];
    //movie_id itu foreign key
    public function movie(){
        return $this->hasMany('App\Movie');
    }
}
