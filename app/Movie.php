<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    protected $fillable = [
        'title', 'sinopsis', 'director', 'date', 'image'
    ];
    public function comments(){
        return $this->hasMany('App\Comment');
    }
    public function cinemas(){
        return $this->belongsTo('App\Cinema', 'cinema_id');
    }
}
