<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'movie_id', 'username', 'comment'
    ];
    //movie_id itu foreign key
    public function movie(){
        return $this->belongsTo('App\Movie', 'movie_id');
    }
}
