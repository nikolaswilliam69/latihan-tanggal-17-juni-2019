<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CommentRequest;
use App\Comment;
use App\Movie;

class CommentController extends Controller
{
    public function commentIndex(){
        $comments = Comment::paginate(6);
        return view('comment', compact('comments'));
    }
    public function commentformIndex(){
        $movies = Movie::all();
        return view('commentform', compact('movies'));
    }
    public function store(CommentRequest $request){
        Comment::create([
            'movie_id'=>$request->movie_id,
            'username'=>$request->username,
            'comment'=>$request->comment
        ]);
        return redirect('/comment')->with('success', 'comment has been added successfully');
    }
    public function edit($id){
        $comment=Comment::findOrfail($id);
        return view('editcomments', compact('comment'));
    }
    public function update(CommentRequest $request,$id){
        Comment::findOrfail($id)->update([
            'username'=>$request->username,
            'comment'=>$request->comment
        ]);
        return redirect('/comment')->with('success', 'Comment Update Success');
    }
    public function delete($id){
        Comment::destroy($id);
        return back()->with('success', 'Comment Deleted');
    }
}
