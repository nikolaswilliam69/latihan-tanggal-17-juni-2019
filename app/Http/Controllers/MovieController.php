<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;
//use Validator;
use App\Http\Requests\MovieRequest;
use Storage;

class MovieController extends Controller
{
    public function movieIndex(){
        //$posts = post::orderBy('title')->get();
        //$posts = post::take(10)->orderBy('title')->get();
        $movies = Movie::paginate(6);
        return view('movie', compact('movies'));
    }
    public function formIndex(){
        return view("form");
    }
    public function store(MovieRequest $request){
        Movie::create([
            'title'=>$request->title,
            'sinopsis'=>$request->sinopsis,
            'director'=>$request->director,
            'date'=>$request->date,
            'image'=>$this->uploadImage($request->file('image'))
        ]);
        return redirect('/movie')->with('success', 'movie has been added successfully');
    }
    //buat upload images pake enctype di form
    public function uploadImage($image){
        $fileNameWithExt = $image->getClientOriginalName();
        $image->storeAs('public/images', $fileNameWithExt);
        return $fileNameWithExt;
    }
    public function edit($id){
        $movie=Movie::findOrfail($id);
        return view('editmovies', compact('movie'));
    }
    public function update(MovieRequest $request,$id){
        Movie::findOrfail($id)->update([
            'title'=>$request->title,
            'sinopsis'=>$request->sinopsis,
            'director'=>$request->director,
            'date'=>$request->date,
            'image'=>$this->uploadImage($request->file('image'))
        ]);
        return redirect('/movie')->with('success', 'Success Update Movie Data');
    }
    public function delete($id){
        Movie::destroy($id);
        // $movie = Movie::findOrfail($id);
        // Storage::delete('/public/images/', $movie->image);
        // $movie->delete();
        return back()->with('success', 'Movie Data Deleted');
    }
    public function show($id){
        $movie=Movie::findOrfail($id);
        return view('showmovie', compact('movie'));
    }
}
    //public function store(Request $request)
            //1.cara traditional
        /*$post = new Post;
        $post->title = $request->title;
        $post->description = $request->description;
        $post->author = $request->author;
        $post->save();
        echo "Success";*/
        //cara 2. query builder
        // DB::table('posts')->insert([
        //     'title'=>$request->title,
        //     'description'=>$request->description,
        //     'author'=>$request->author
        // ]);
        // echo 'success with query builder';
        //cara pake validasi
        //cara 1 validasi
        // $validator = validator::make($request->all(), [
        //     'title'=>'required|min: 5',
        //     'description'=>'required|max: 100',
        //     'author'=>'required'
        // ]);
        // if($validator->fails()){
        //     return back()->withErrors($validator->errors())->withInput();
        // }
        //cara 2 validasi
        // $request->validate([
        //     'title'=>'required|min: 5',
        //     'description'=>'required|max: 100',
        //     'author'=>'required'
        // ]);
        //cara 3 validasi(pakai request(php artisan make:request PostRequest))
        //lanjutin cara create: cara 3.eloquent(musti pakai model(liat post.php))