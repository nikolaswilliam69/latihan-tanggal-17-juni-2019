<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cinema;
use App\Movie;
use App\Http\Requests\CinemaRequest;

class CinemaController extends Controller
{
    public function cinemaIndex(){
        $cinemas = Cinema::paginate(6);
        return view('cinema', compact('cinemas'));
    }
    public function cinemaformIndex(){
        $movies = Movie::all();
        return view('cinemaform', compact('movies'));
    }
    public function store(CinemaRequest $request){
        Cinema::create([
            'cinema'=>$request->cinema,
            'address'=>$request->address
        ]);
        return redirect('/cinema')->with('success', 'cinema has been added successfully');
    }
    public function edit($id){
        $cinema=Cinema::findOrfail($id);
        return view('editcinemas', compact('cinema'));
    }
    public function update(CinemaRequest $request,$id){
        Cinema::findOrfail($id)->update([
            'cinema'=>$request->cinema,
            'address'=>$request->address
        ]);
        return redirect('/cinema')->with('success', 'Cinema Update Success');
    }
    public function delete($id){
        Cinema::destroy($id);
        return back()->with('success', 'Cinema Deleted');
    }
}
