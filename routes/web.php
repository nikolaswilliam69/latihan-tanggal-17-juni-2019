<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//ada 6 metode
Route::get('/', 'WelcomeController@index');
Route::get('/post', 'PostController@postIndex');
Route::get('/comment', 'CommentController@commentIndex');
Route::get('/createcomment', 'CommentController@commentformIndex');
Route::post('/createcomment', 'CommentController@store')->name('comment.add');
Route::get('/comment/{id}', 'CommentController@edit')->name('comment.edit');
Route::patch('/comment/{id}', 'CommentController@update')->name('comment.update');
Route::delete('/comment/{id}', 'CommentController@delete')->name('comment.delete');
Route::get('/movie', 'MovieController@movieIndex');
Route::get('/form', 'MovieController@formIndex');
Route::get('/movie/show/{id}', 'MovieController@show')->name('movie.show');
Route::post('/form','MovieController@store')->name('movie.add');
Route::get('/movie/{id}', 'MovieController@edit')->name('movie.edit');
Route::patch('/movie/{id}', 'MovieController@update')->name('movie.update');
Route::delete('/movie/{id}', 'MovieController@delete')->name('movie.delete');
Route::get('/cinema', 'CinemaController@cinemaIndex');
Route::get('/createcinema', 'CinemaController@cinemaformIndex');
Route::post('/createcinema', 'CinemaController@store')->name('cinema.add');
Route::get('/cinema/{id}', 'CinemaController@edit')->name('cinema.edit');
Route::patch('/cinema/{id}', 'CinemaController@update')->name('cinema.update');
Route::delete('/cinema/{id}', 'CinemaController@delete')->name('cinema.delete');