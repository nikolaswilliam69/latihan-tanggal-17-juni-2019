<?php

use Illuminate\Database\Seeder;

class CommentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->insert([
            'id'=>'121347',
            'username'=>'LilBrat',
            'comment'=>'LOL'
        ]);
        DB::table('comments')->insert([
            'id'=>'122367',
            'username'=>'TeriyakiGood',
            'comment'=>'OmegaLul'
        ]);
        DB::table('comments')->insert([
            'id'=>'123789',
            'username'=>'ToyingWithMaiden',
            'comment'=>'Crap'
        ]);
    }
}