<?php

use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            'title'=>'StarWars',
            'description'=>'Perang Bintang',
            'author'=>'J.J Abrams'
        ]);
        DB::table('posts')->insert([
            'title'=>'Batman',
            'description'=>'Manusia Kelelawar',
            'author'=>'DC Comics'
        ]);
        DB::table('posts')->insert([
            'title'=>'Godzilla',
            'description'=>'Monster Nuklir',
            'author'=>'Legendary'
        ]);
    }
}
