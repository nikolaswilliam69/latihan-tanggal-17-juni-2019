<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cinema_id')->unsigned();
            //foreign key bakal baca id di kategori cinemas
            $table->foreign('cinema_id')->references('id')->on('cinemas')->onDeleteCascade();
            $table->string('title');
            $table->string('sinopsis');
            $table->string('director');
            $table->string('date');
            $table->string('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
